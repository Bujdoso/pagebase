# Pagebase 

Pagebase is a blog engine, loosely modelled after Wordpress. 
It offers the same core feature set.

## Roadmap

Migrated from BitBucket to GitLab.
Bring it up to the latest Golang version
Consider Gorilla for HTML templating
Potentially remove the database and serve pages from memory.


## License

Pagebase is licensed under GNU 3.0.

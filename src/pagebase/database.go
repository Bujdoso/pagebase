package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"io/ioutil"
	"strings"
)

type Database struct {
	Host   string
	Port   string
	User   string
	Pass   string
	Scheme string

	Works bool

	db *sql.DB
}

func (d *Database) init() error {
	var err error

	d.Works = false

	fmt.Printf("Initializing database connection...\n")

	fmt.Printf("Picked up database config as: %s:%s@%s:%s@/%s\n", d.Host, d.Port, d.User, d.Pass, d.Scheme)
	ConnStr := d.User + ":" + d.Pass + "@/" + d.Scheme

	d.db, err = sql.Open("mysql", ConnStr)

	if err != nil {
		fmt.Printf("Opening database failed! (%s)\n", err)
		return err
	}

	err = d.db.Ping()
	if err != nil {
		fmt.Printf("Database connection is refused! (%s)", err)
		return err
	}
	d.Works = true
	fmt.Printf("Database connection is established.\n")

	return nil
}

// There will be a proper JSON (or other kind) model declaration once.
// For now it's hardwired to the blog.

func (d *Database) create() error {
	SchemaContent, err := ioutil.ReadFile("pagebase.schema")
	if err != nil {
		fmt.Printf("Can't open schema file!\n")
		return err
	}
	fmt.Printf("Initializing database....\n")
	SchemaLines := strings.Split(string(SchemaContent), "\n")

	for _, ConfLine := range SchemaLines {
		_, err := d.db.Query(ConfLine)
		if err != nil {
			//			fmt.Printf("Error in schema line: %s\n", i)
			fmt.Printf("SQL statement is rejected with: %s\n", err)
			//			return err
		}

	}
	_, err = d.db.Query("insert into users (user, password, flags) values ('" + Config["Admin"] + "', MD5('" + Config["AdminPass"] + "'), 1);")

	if err != nil {
		fmt.Printf("Admin user creation failed. It probably already exists.\n")
		return err
	}

	return nil
}

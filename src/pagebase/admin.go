package main

import (
	"fmt"
	//	"flag"
	"html/template"
	//	"io/ioutil"
	"io"
	"log"
	//	"net"
	"net/http"
	//	"regexp"
	"crypto/rand"
	"encoding/base64"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"
	//	"database/sql"
	//	_ "github.com/go-sql-driver/mysql"
)

type ExtEntry struct {
	Id      int
	State   int
	Content string
	Value   string
}

type PostEntry struct {
	Id         int
	Title      string
	Body       template.HTML
	Permalink  string
	PostedOn   string
	PosterId   int
	Categories []string
	Tags       []string
	Ext        int
	Private    string
	ExtData    map[string][]ExtEntry
}

type PageFooter struct {
	PageLeftLink	template.HTML
	PageRightLink	template.HTML
	Tags		[]string			// for tag clouds, etc
	Categories	[]string		// for category lists
	Recents		[]PostEntry		// recent posts in the sidebar or footer
	Archives	[]string		// links for post archives
}

type TopMenu struct {
	Menu template.HTML
}

var PostCount int

// -------- Pull all tags/cols (depending on colName) into a special post entry
// Currently for id, name tables only - but saves code

func adminPullAll(d *Database, colName string) []ExtEntry {
	var tExtSlice []ExtEntry

	rows, err := d.db.Query("Select id, value from taxmeta where name='" + colName + "'")
	defer rows.Close()
	if err != nil {
		log.Fatal(err)
	}

	tExt := ExtEntry{}
	for rows.Next() {
		var name string
		var id int

		rows.Scan(&id, &name)
		tExt.Id = id
		tExt.Value = name
		tExtSlice = append(tExtSlice, tExt)
	}
	return tExtSlice
}

// --- Fill up a PostEntry Tags and Categories slice (of strings) from the relational tables (for post specified in id)
// --- if id is zero, return all tags
func adminPullTags(d *Database, id int) []string {
	var Tag string
	TagList := []string{}
	var tc int

	if id == 0 {
		rows, _ := d.db.Query("select value from taxmeta where name='PB_TAG'")
		for rows.Next() {
			rows.Scan(&Tag)
			TagList = append(TagList, Tag)
		}
		return TagList
	}


	err := d.db.QueryRow("SELECT Count(*) from taxmeta where name='PB_TAG'").Scan(&tc)
	if err != nil {
		log.Fatal(err)
	}
	if tc > 0 {
		rows, _ := d.db.Query("select distinct b.value from taxmeta a, taxmeta b where a.name='PB_TAG_REL' and a.parent=? and a.value=b.id;", id)
		for rows.Next() {
			rows.Scan(&Tag)
			TagList = append(TagList, Tag)
		}
	}
	return TagList
}


// Same here, but for categories. 0 indicates all cats.
func adminPullCats(d *Database, id int) []string {
	var Cat string
	CatList := []string{}
	var cc int

	if id == 0 {
		rows, _ := d.db.Query("select value from taxmeta where name='PB_CAT'")
		for rows.Next() {
			rows.Scan(&Cat)
			CatList = append(CatList, Cat)
		}
		return CatList
	}


	err := d.db.QueryRow("SELECT Count(*) from taxmeta where name='PB_CAT'").Scan(&cc)
	if err != nil {
		log.Fatal(err)
	}
	if cc > 0 {
		rows, _ := d.db.Query("select distinct b.value from taxmeta a, taxmeta b where a.name='PB_CAT_REL' and a.parent=? and a.value=b.id;", id)

		for rows.Next() {
			rows.Scan(&Cat)
			CatList = append(CatList, Cat)
		}
	}
	return CatList
}

func adminHandler(w http.ResponseWriter, r *http.Request, title string, s *Session, d *Database) {
	var SessionValid bool
	// -----------------------------------------------------------------------
	// Checks if cookie is present in the headers
	// and it's the same as the stored one

	fmt.Printf("[admin] access attempt from %v for /admin/%v\n", r.RemoteAddr, title)

	if VCookie, ok := r.Header["Cookie"]; ok {
		SCookie := VCookie[0]
		SCookieTmp := strings.Split(SCookie, "=")[1] + "="

		//		fmt.Printf("Getting Cookie from header: %s\n", SCookieTmp)
		//		fmt.Printf("Checking: stored:%s got:%s\n", s.ID, SCookieTmp)

		if SCookieTmp == s.ID {
			SessionValid = true
		} else {
			SessionValid = false
			fmt.Printf("[admin] Cookies don't match: %s <-> %s\n", s.ID, SCookieTmp)
		}

	} else {
		SessionValid = false
	}

	// -------------------------------------------------------------------------
	// /admin/logout invalidates your session

	if title == "logout" {
		s.ID = ""
		SessionValid = false
	}

	// -------------------------------------------------------------------------
	// /admin/login renders the login page or redirects
	// to the main page if valid session is found

	if title == "login" {
		if SessionValid == true {
			http.Redirect(w, r, "/admin/main", http.StatusFound)
			return
		} else {
			renderLoginTemplate(w, "login", nil)
			return
		}
	}

	// --------------------------------------------------------------------------
	// /admin/dologin is trying to log you in
	// and generates a session ID

	if title == "dologin" {
		if SessionValid == false {
			fmt.Printf("Validating admin session for %v\n", r.RemoteAddr)

			if (r.FormValue("user") != Config["AdminUser"]) || (r.FormValue("pass") != Config["AdminPass"]) {
				fmt.Printf("[admin] Denied admin access to %v\n", r.RemoteAddr)
				http.Redirect(w, r, "/admin/login", http.StatusFound)
				return
			}

			// 	Let's suppose you logged in (for now)
			fmt.Printf("[admin] Granted admin access to %v\n", r.RemoteAddr)

			expire := time.Now().AddDate(0, 0, 1)

			rb := make([]byte, 32)
			_, err := rand.Read(rb)
			if err != nil {
				fmt.Println(err)
			}
			SessionID := base64.URLEncoding.EncodeToString(rb)
			//			fmt.Printf("Storing generated session id:%s\n", SessionID)
			s.ID = SessionID
			NewCookie := http.Cookie{"pagebase",
				SessionID,
				"/",
				Config["Hostname"],
				expire,
				expire.Format(time.UnixDate),
				86400,
				false,
				true,
				"pagebase=pagebase",
				[]string{"pagebase=pagebase"}}
			http.SetCookie(w, &NewCookie)
			//			fmt.Printf("Setting up new session.\n")

			http.Redirect(w, r, "/admin/main", http.StatusFound)
			return
			//				renderAdminTemplate(w, "admin_main", nil)
		}
	}
	// --------------------------------------------------------------------------
	// blanket check: if you still don't have session, you're out.

	if SessionValid == false {
		http.Redirect(w, r, "/admin/login", http.StatusFound)

	} else {

		if title == "main" {
			m := make(map[string]string)
			var tmp string

			mem := &runtime.MemStats{}
			runtime.ReadMemStats(mem)
			m["stat_go_alloc"] = strconv.FormatUint(mem.Alloc, 10)
			m["stat_go_version"] = runtime.Version()
			m["stat_os_version"] = runtime.GOOS

			err := d.db.QueryRow("SELECT version()").Scan(&tmp)
			if err != nil {
				log.Fatal(err)
			}
			m["stat_mysql_version"] = tmp

			err = d.db.QueryRow("SELECT count(*) from posts").Scan(&tmp)
			m["stat_post_count"] = tmp

			renderAdminMapTemplate(w, "admin_main", m)
			return
		}
		// --------------------------------------------------------------------------
		// /admin/posts lists the existing posts and lets you to edit,delete,tag them

		if title == "postlist" {
			var title, posted_on string
			var id int
			var filter string
			var private int

			// default filter is "All"
			filter = "SELECT id, title, posted_on, private from posts " + filter + "order by posted_on desc"

			// There are possible filters for these. Only one of them is acccepted at a time:

			search_filter := r.URL.Query().Get("keyword_filter")
			date_filter := r.URL.Query().Get("date_filter")
			//			 category_filter:= r.URL.Query().Get("category_filter")
			tag_filter := r.URL.Query().Get("tag_filter")

			if len(search_filter) > 0 {
				filter = fmt.Sprintf("SELECT id, title, posted_on, private from posts where body like '%%%v%%' order by posted_on desc", search_filter)
			}
			if len(date_filter) > 0 {
				date_refilter := date_filter[0:4] + date_filter[5:7]
				filter = fmt.Sprintf("SELECT id, title, posted_on, private from posts where EXTRACT(YEAR_MONTH FROM posted_on)=%v order by posted_on desc", date_refilter)
			}
			if len(tag_filter) > 0 {
				filter = fmt.Sprintf("select a.id, a.title, a.posted_on, private from posts a, taxmeta b where b.name='PB_TAG_REL' and a.id=b.parent and b.value=%v", tag_filter)
			}

			//			fmt.Printf("filter: %v\n", filter)

			tRes := PostEntry{}
			PostList := []PostEntry{}

			err := d.db.QueryRow("SELECT Count(*) from posts").Scan(&PostCount)
			if err != nil {
				log.Fatal(err)
			}

			if PostCount == 0 {
				tRes.Title = "Sorry,"
				tRes.Body = "You have no posts."
				PostList = append(PostList, tRes)
			} else {
				//				PostList = make([]PostEntry, 10)
				rows, err := d.db.Query(filter)
				defer rows.Close()
				if err != nil {
					log.Fatal(err)
				}
				for rows.Next() {
					rows.Scan(&id, &title, &posted_on, &private)
					tRes.Id = id
					tRes.Title = title
					//tRes.Body = template.HTML(body)
					tRes.PostedOn = posted_on
					tRes.PosterId = 1
					tRes.Ext = 0

					tRes.Private = "no"
					if private == 1 {
						tRes.Private = "yes"
					}

					tRes.Categories = adminPullCats(d, id)
					tRes.Tags = adminPullTags(d, id)
					PostList = append(PostList, tRes)
				}
			}

			// --- This is a special PostEntry to store all the possible cats and tags.
			tRes.Categories = nil
			tRes.Tags = nil

			tRes.Ext = 1
			tRes.ExtData = make(map[string][]ExtEntry)
			tRes.ExtData["Tags"] = []ExtEntry{}
			tRes.ExtData["Cats"] = []ExtEntry{}

			tRes.ExtData["Tags"] = adminPullAll(d, "PB_TAG")
			tRes.ExtData["Cats"] = adminPullAll(d, "PB_CAT")
			// ------- Append this to the PostList too
			PostList = append(PostList, tRes)
			renderAdminTemplate(w, "admin_post_list", PostList)
			return
		}

		if title == "postadd" {
			tRes := PostEntry{}
			tRes.ExtData = make(map[string][]ExtEntry)
			tRes.ExtData["Tags"] = []ExtEntry{}
			tRes.ExtData["Cats"] = []ExtEntry{}
			tRes.ExtData["Tags"] = adminPullAll(d, "PB_TAG")
			tRes.ExtData["Cats"] = adminPullAll(d, "PB_CAT")
			renderAdminEditTemplate(w, "admin_post_add", tRes)
			return
		}

		if title == "postedit" {
			var title, body string
			var private int
			tRes := PostEntry{}

			id := r.URL.Query().Get("id")

			err := d.db.QueryRow("SELECT id, title, body, private from posts where id=?", id).Scan(&id, &title, &body, &private)
			if err != nil {
				log.Fatal(err)
			}
			tRes.Id, _ = strconv.Atoi(id)
			tRes.Title = title
			tRes.Body = template.HTML(body)

			tRes.ExtData = make(map[string][]ExtEntry)
			tRes.ExtData["Tags"] = []ExtEntry{}
			tRes.ExtData["Cats"] = []ExtEntry{}

			tRes.ExtData["Tags"] = adminPullAll(d, "PB_TAG")
			tRes.ExtData["Cats"] = adminPullAll(d, "PB_CAT")

			tRes.Categories = adminPullCats(d, tRes.Id)
			tRes.Tags = adminPullTags(d, tRes.Id)

			for i, cat := range tRes.ExtData["Cats"] {
				for _, useCat := range tRes.Categories {
					if cat.Value == useCat {
						tRes.ExtData["Cats"][i].State = 1
					}
				}
			}

			tRes.Private = ""
			if private == 1 {
				tRes.Private = "checked"
			}

			renderAdminEditTemplate(w, "admin_post_edit", tRes)
			return
		}

		if title == "postsave" {
			var PostId int

			PostUpdate := r.FormValue("update")
			PostTitle := r.FormValue("title")
			PostContent := r.FormValue("content")
			iPostPrivate := r.FormValue("private")

			PostPrivate := 0
			if iPostPrivate == "private" {
				PostPrivate = 1
			} else {
				PostPrivate = 0
			}

			t := time.Now()
			iMonth := int(t.Month())
			PermaDate := strconv.Itoa(t.Year()) + "/" + strconv.Itoa(iMonth)
			PermaLink := "/" + PermaDate + "/" + strings.Replace(PostTitle, " ", "-", -1)

			if PostUpdate == "true" {
				tId := r.FormValue("id")
				PostId, _ = strconv.Atoi(tId)
				_, err := d.db.Exec("UPDATE posts set title=?,body=?, permalink=?, posted_on=posted_on, private=? where id=?", PostTitle, PostContent, PermaLink, PostPrivate, PostId)
				if err != nil {
					log.Fatal(err)
				}
				fmt.Printf("[admin] Updated post id %v\n", PostId)
				// --- BIG TODO: compare post-cat relations and insert/delete if there is a difference compared to the form
				r.ParseForm()
				// --- BIG TODO#2: same for tags

			} else {
				// --------------------------------- This is inserting a brand new post. Much easier.
				_, err := d.db.Exec("INSERT INTO posts (title,body,posted_on, permalink, private) values (?,?,NOW(),?,?)", PostTitle, PostContent, PermaLink, PostPrivate)
				if err != nil {
					log.Fatal(err)
				}
				fmt.Printf("[admin] Added new post titled: %v\n", PostTitle)

				r.ParseForm()

				// ---- Get last inserted post Id
				err = d.db.QueryRow("SELECT LAST_INSERT_ID()").Scan(&PostId)
				if err != nil {
					log.Fatal(err)
				}
			}

			// ---------- Handling tags

			TagLine := r.FormValue("tags")

			// ---- Loop through tags to see if it exists. Insert if it isn't.
			// ---- Scratch that. Let's get rid of tags first!
			_, err := d.db.Exec("DELETE FROM taxmeta where parent=? and name='PB_TAG_REL'", PostId)
			if err != nil {
				log.Fatal(err)
			}

			if len(TagLine) > 0 {
				var tagId int
				var tagIds []int

				// ------------------- We only need to work with tags if the post is tagged, otherwise we don't!
				TagLines := strings.Split(TagLine, " ")
				var tagFound int

				for _, tag := range TagLines {
					err := d.db.QueryRow("select count(*) from taxmeta where name='PB_TAG' and value=?", tag).Scan(&tagFound)

					if err != nil {
						log.Fatal(err)
					}
					if tagFound == 0 { // it means that this tag doesn't exist yet
						_, err := d.db.Exec("INSERT INTO taxmeta (name, type, value) values ('PB_TAG','R',?)", tag)
						if err != nil {
							log.Fatal(err)
						}
					}
					// since we're here we collect all tag ids into a slice
					err = d.db.QueryRow("select id from taxmeta where name='PB_TAG' and value=?", tag).Scan(&tagId)

					if err != nil {
						log.Fatal(err)
					}
					tagIds = append(tagIds, tagId)
				}

				// ---- we have a post id, tag id - let's build post-tag relations
				for _, tag := range tagIds {
					_, err := d.db.Exec("INSERT INTO taxmeta (name, type, parent, value) VALUES ('PB_TAG_REL','L',?,?)", PostId, tag)
					if err != nil {
						log.Fatal(err)
					}
				}
			}

			// ---- iterating through the checkboxes called "cat" (categories)
			// ---- no need to check if category exists or not, just (de)assign it from the post

			_, err = d.db.Exec("DELETE FROM taxmeta where parent=? and name='PB_CAT_REL'", PostId)
			if err != nil {
				log.Fatal(err)
			}

			for _, cat := range r.Form["cat"] {
				_, err := d.db.Exec("INSERT INTO taxmeta (name, type, parent, value) VALUES ('PB_CAT_REL','L',?,?)", PostId, cat)
				if err != nil {
					log.Fatal(err)
				}
			}

			// ------ now same for the categories - we just don't have to check if they exist just build the relations.
			//			for fname, vname := range r.Form.Values {
			//				fmt.Printf("%v %v\n", fname, vname)

			//			}

			//fmt.Printf("Post added/updated from %v\n", r.RemoteAddr)
			http.Redirect(w, r, "/admin/postlist", http.StatusFound)
			return
		}

		if title == "postdel" {
			id := r.URL.Query().Get("id")
			_, err := d.db.Exec("DELETE from posts where id=?", id)
			fmt.Printf("[admin] Deleted post id %v by %v\n", id, r.RemoteAddr)

			if err != nil {
				log.Fatal(err)
			}
			http.Redirect(w, r, "/admin/postlist", http.StatusFound)
			return
		}

		if title == "imagelist" {

		}

		if title == "imageadd" {
			renderAdminTemplate(w, "admin_image_add", nil)
			return
		}

		if title == "imagesave" {
			var ImageFullPath string
			var iMonth int

			t := time.Now()
			iMonth = int(t.Month())

			//			ImageFile, header, err := r.FormFile("image")
			ImageTitle := r.FormValue("title")
			ImagePath := "static/" + strconv.Itoa(t.Year()) + "/" + strconv.Itoa(iMonth)
			err := os.MkdirAll(ImagePath, 0775)
			if err != nil {
				log.Fatal(err)

			}
			err = r.ParseMultipartForm(100000)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			m := r.MultipartForm

			files := m.File["file"]
			for i, _ := range files {
				//for each fileheader, get a handle to the actual file
				file, err := files[i].Open()
				defer file.Close()
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				//create destination file making sure the path is writeable.
				ImageFullPath = ImagePath + "/" + files[i].Filename
				dst, err := os.Create(ImageFullPath)
				defer dst.Close()
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				//copy the uploaded file to the destination file
				if _, err := io.Copy(dst, file); err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}

			}
			fmt.Printf("[admin] Uploaded image to %v\n", ImageFullPath)

			_, err = d.db.Exec("INSERT INTO images (title, path, upped_on) VALUES (?,?, NOW())", ImageTitle, ImageFullPath)
			if err != nil {
				log.Fatal(err)
				return
			}

			//renderAdminTemplate(w, "admin_main", nil)
			fmt.Fprintf(w, "<html><head><script language=\"javascript\" type=\"text/javascript\">")
			fmt.Fprintf(w, "window.parent.window.jbImagesDialog.uploadFinish({")
			fmt.Fprintf(w, "filename:'/%v', result: 'file_uploaded', resultCode: 'ok'})", ImageFullPath)
			fmt.Fprintf(w, "</script>")
			fmt.Fprintf(w, "<style type=\"text/css\">body {font-family: Courier, \"Courier New\", monospace; font-size:11px;}</style>")
			fmt.Fprintf(w, "</head><body>Result: file_uploaded</body></html>")

			return
		}

		if title == "viewedit" {
			tRes := PostEntry{}

			tRes.ExtData = make(map[string][]ExtEntry)

			tRes.ExtData["Tags"] = []ExtEntry{}
			tRes.ExtData["Cats"] = []ExtEntry{}

			tRes.ExtData["Tags"] = adminPullAll(d, "PB_TAG")
			tRes.ExtData["Cats"] = adminPullAll(d, "PB_CAT")

			renderAdminEditTemplate(w, "admin_view_edit", tRes)
		}

		if title == "vieweditmod" {
			root_cat_filter := r.FormValue("root_view_cat_filter")
			root_tag_filter := r.FormValue("root_view_tag_filter")
			grid_cat_filter := r.FormValue("grid_view_cat_filter")
			grid_tag_filter := r.FormValue("grid_view_tag_filter")

			// This one nukes all the filters
			_, err := d.db.Exec("delete from taxmeta where name='PB_VIEW_TAG_FILTER'")
			_, err = d.db.Exec("delete from taxmeta where name='PB_VIEW_CAT_FILTER'")

			_, err = d.db.Exec("insert into taxmeta (name,value,type,parent) VALUES ('PB_VIEW_CAT_FILTER','root','R',?)", root_cat_filter)
			_, err = d.db.Exec("insert into taxmeta (name,value,type,parent) VALUES ('PB_VIEW_TAG_FILTER','root','R',?)", root_tag_filter)
			_, err = d.db.Exec("insert into taxmeta (name,value,type,parent) VALUES ('PB_VIEW_CAT_FILTER','grid','R',?)", grid_cat_filter)
			_, err = d.db.Exec("insert into taxmeta (name,value,type,parent) VALUES ('PB_VIEW_TAG_FILTER','grid','R',?)", grid_tag_filter)

			if err != nil {
				log.Fatal(err)
			}
			http.Redirect(w, r, "/admin/main", http.StatusFound)
		}

		if title == "tagcatedit" {
			tRes := PostEntry{}

			tRes.ExtData = make(map[string][]ExtEntry)

			tRes.ExtData["Tags"] = []ExtEntry{}
			tRes.ExtData["Cats"] = []ExtEntry{}

			tRes.ExtData["Tags"] = adminPullAll(d, "PB_TAG")
			tRes.ExtData["Cats"] = adminPullAll(d, "PB_CAT")

			renderAdminEditTemplate(w, "admin_tagcat_edit", tRes)

		}

		if title == "tagcatadd" {
			amode := r.URL.Query().Get("mode")

			if amode == "tag" {
				tag := r.FormValue("tag")
				if len(tag) > 0 {
					_, err := d.db.Exec("insert into taxmeta (name,value,type) VALUES ('PB_TAG',?,'R')", tag)
					if err != nil {
						log.Fatal(err)
					}

				}
			}

			if amode == "cat" {
				cat := r.FormValue("cat")
				if len(cat) > 0 {
					_, err := d.db.Exec("insert into taxmeta (name,value,type) VALUES ('PB_CAT',?,'R')", cat)
					if err != nil {
						log.Fatal(err)
					}
				}
			}

			http.Redirect(w, r, "/admin/tagcatedit", http.StatusFound)
		}

		if title == "logout" {
			SessionValid = false
			http.Redirect(w, r, "/admin", http.StatusFound)
			return
		}

	}

}

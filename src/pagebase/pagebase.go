// Copyright 2019 Artur Bujdoso. All rights reserved
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"regexp"
	//	"time"
	//	"encoding/base64"
	//	"crypto/rand"
	//	"strings"
	//	"database/sql"
	//	_ "github.com/go-sql-driver/mysql"
	"strconv"
	"strings"
	"bytes"
)

var (
	addr = flag.Bool("addr", false, "find open address and print to final-port.txt")
)

const (
	PRIV_KEY   = "./private_key"
	PUBLIC_KEY = "./public_key"
)

type Page struct {
	ID       int
	Title    string
	Body     []byte
	PostDate string
}

type Tag struct {
	ID   int
	Name string
}

type Image struct {
	ID       int
	FileName string
	Title    string
}

type Session struct {
	ID string
}

type menuItem struct {
	id int		// id of this item
	prev int	// id of previous item or -1 if none
	next int	// id of next item or -1 if none
	sub int		// id of submenu opening from this one or -1 if none
	url string	// URL of this menu pointing to or "" if none
	label string	// label to be shown
}



var SSession *Session
var d *Database

var title, body string

var postFields string
var PostPerPage int


var menuText bytes.Buffer
var menuTree []menuItem

func (p *Page) save() error {
	filename := p.Title + ".txt"
	return ioutil.WriteFile(filename, p.Body, 0600)
}

func loadPage(title string) (*Page, error) {
	filename := title + ".txt"
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return &Page{Title: title, Body: body}, nil
}


/*
	This function returns a string with the generated menu html in it
*/
func getMenu() string {
	return menuText.String()

}

/*
	It generates the menu html by recursively traversing the menu list pulled from db
*/
func renderMenu(i int) {
	var q,l,m int
	q = 0

	for l = 0; l < len(menuTree) ; l++ {
		if menuTree[l].id == i {
			m = l;
		}
	}


	for q != 1 {
		if menuTree[m].sub != -1 {
			menuText.WriteString(fmt.Sprintf("<li id=\"menu-item-%v\" class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-%v\">", menuTree[m].id, menuTree[m].id))
			menuText.WriteString(fmt.Sprintf("<a href=\"%v\">%v</a>\n", menuTree[m].url, menuTree[m].label))
			menuText.WriteString(fmt.Sprintf("<ul class=\"sub-menu\">\n"))

			renderMenu(menuTree[m].sub)

			menuText.WriteString("</li>\n")
			menuText.WriteString("</ul>\n")
		}

		if menuTree[m].sub == -1 {
			menuText.WriteString(fmt.Sprintf("<li id=\"menu-item-%v\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-%v\">", menuTree[m].id, menuTree[m].id))
			menuText.WriteString(fmt.Sprintf("<a href=\"%v\">%v</a></li>\n", menuTree[m].url, menuTree[m].label))
		}

		if menuTree[m].next == -1 && menuTree[m].sub == -1 {
			q = 1
		} else {
			i = menuTree[m].next
			for l = 0 ; l < len(menuTree) ; l++ {
				if menuTree[l].id == i {
					m = l;
				}
			}
		}
	}

}

/*
	This function pulls the menu from the database so it can be traversed recursively
	and also acts as a cache for less db load
*/
func loadMenu() {
	var menuTemp menuItem

	rows, err := d.db.Query("SELECT id, prev, next, sub, url, label from menus")

	defer rows.Close()
	if err != nil {
		log.Fatal(err)
	}
	// pulling the menu from database so we can recurse into it
	for rows.Next() {
		rows.Scan(&menuTemp.id, &menuTemp.prev, &menuTemp.next, &menuTemp.sub, &menuTemp.url, &menuTemp.label)
		menuTree = append(menuTree, menuTemp)
	}

	menuText.Reset()

	menuText.WriteString("<ul id=\"menu-menu-1\" class=\"menu\">\n")
	renderMenu(1) // start rendering the menu from the first element
	menuText.WriteString("</ul>\n")


	return
}



/*
	This handler renders a template based on the request.
	It's a little monolithic yet.
*/

func pageHandler(w http.ResponseWriter, r *http.Request, title string, s *Session, d *Database) {
	tRes := PostEntry{}
	PostList := []PostEntry{}

	var LinkPrefix string

	pf := PageFooter{}
	pm := TopMenu{}

	rows, err := d.db.Query("SELECT 1")

	var archYear int
	var archMonth int
	var archDay int
	var archPlink string

	archPath := strings.Split(title, "/") // parsing calling path
	pathLen := len(archPath) - 1

	// Defaults for paging

	PageOffs := 0
	PagePrev := "1"
	PageNext := ""
	PostFrom := 0
	PostTotal := 0

	// If we got this far, we're already in "/archive or /page"

	//	archPage := 0

	// ATTACK VECTOR - parameters could be SQL statements so converting them to integer first.
	// Worst case: parameters will be converted to "0".

	// called with empty path: "/"
	if pathLen == 0 {
		LinkPrefix = ""
		PageOffs = -1
	}

	// called with either "archive" or a specified keyword....
	if pathLen == 1 {

	}

	// only year specified: /archive/YYYY - OR - page/N
	if pathLen == 2 {
		if archPath[1] == "archive" {
			archYear, _ = strconv.Atoi(archPath[2])
			LinkPrefix = fmt.Sprintf("<a href=\"/archive/%v/page/", archYear)

			err = d.db.QueryRow("SELECT count(*) from posts where YEAR(posted_on)=? and private=0", archYear).Scan(&PostTotal)
			rows, err = d.db.Query("SELECT "+postFields+" from posts where YEAR(posted_on)=? and private=0 order by posted_on desc limit 0,?", archYear, PostPerPage)
		}

		if archPath[1] == "page" {
			PageOffs, _ = strconv.Atoi(archPath[2])
			PostFrom = PageOffs * PostPerPage

			LinkPrefix = "<a href=\"/page/"
			err = d.db.QueryRow("SELECT count(*) from posts where private=0").Scan(&PostTotal)
			rows, err = d.db.Query("SELECT "+postFields+" from posts where private=0 order by posted_on desc limit ?,?", PostFrom, PostPerPage)
		}

		// "grid" view. Right now hardcoded.
		// Pagination is disabled with zero post offset and 255 posts per page.

		if archPath[1] == "grid" {
			PostFrom = 0
			PostPerPage = 255
			PageOffs = -1
			stream_tag := 0
			err = d.db.QueryRow("SELECT parent from taxmeta where name='PB_VIEW_TAG_FILTER' and value='grid'").Scan(&stream_tag)
			// Remember to protect the single alphabet letter!
			rows, err = d.db.Query("select posts.*,taxmeta.value, taxmeta.name, taxmeta.parent from posts,taxmeta "+
				"where posts.id=taxmeta.parent and taxmeta.name='PB_TAG_REL' and taxmeta.value=? and LEFT(posts.title, 1) = \""+archPath[2]+"\"", stream_tag)
		}

	}

	// specified: /archive/YYYY/MM	- OR - /archive/page/N
	if pathLen == 3 {
		LinkPrefix = "<a href=\"/archive/page/"

		if archPath[2] == "page" {
			PageOffs, _ = strconv.Atoi(archPath[3])

			PostFrom = PageOffs * PostPerPage
			err = d.db.QueryRow("SELECT count(*) from posts").Scan(&PostTotal)
			rows, err = d.db.Query("SELECT "+postFields+" from posts where private=0 limit ?,?", PostFrom, PostPerPage)
		} else {
			archYear, _ = strconv.Atoi(archPath[2])
			archMonth, _ = strconv.Atoi(archPath[3])
			LinkPrefix = fmt.Sprintf("<a href=\"/archive/%v/%v/page/", archYear, archMonth)
			err = d.db.QueryRow("SELECT count(*) from posts where YEAR(posted_on)=? and MONTH(posted_on)=? and private=0", archYear, archMonth).Scan(&PostTotal)
			rows, err = d.db.Query("SELECT "+postFields+" from posts where YEAR(posted_on)=? and MONTH(posted_on)=? and private=0 order by posted_on desc", archYear, archMonth)
		}
	}

	// specified: /archive/YYYY/MM/DD  - OR - /archive/YYYY/MM/perm-a-link  - OR - /archive/YYYY/page/N
	if pathLen == 4 {
		LinkPrefix = "" // no paging if there is a permalink
		PageOffs = -1   // .... or specific daily posts
		archYear, _ = strconv.Atoi(archPath[2])
		archMonth, err = strconv.Atoi(archPath[3])
		if err != nil { // if it's not the month it's "page" - or we assume it is - also it's a paged year archive then
			PageOffs, _ = strconv.Atoi(archPath[4])
			PostFrom = PageOffs * PostPerPage
			LinkPrefix = fmt.Sprintf("<a href=\"/archive/%v/page/", archYear)
			err = d.db.QueryRow("SELECT count(*) from posts where YEAR(posted_on)=? private=0", archYear).Scan(&PostTotal)
			rows, err = d.db.Query("SELECT "+postFields+" from posts where YEAR(posted_on)=? and private=0 order by posted_on desc limit ?,?", archYear, PostFrom, PostPerPage)
		} else {
			// if 3rd parameter is a number then we use it as a day
			// if it's not we look it up as a permalink
			archDay, err = strconv.Atoi(archPath[4])
			if err != nil { // it's a permalink - which has the format of /YYYY/MM/perm-a-link (!!!)
				// ATTACK VECTOR: back-and-forth string-int-string conversion to make SQL injection harder.
				archPlink = "/" + strconv.Itoa(archYear) + "/" + strconv.Itoa(archMonth) + "/" + archPath[4]
				err = d.db.QueryRow("SELECT count(*) from posts where YEAR(posted_on)=? and MONTH(posted_on)=? and permalink=? and private =0", archYear, archMonth, archPlink).Scan(&PostTotal)
				rows, err = d.db.Query("SELECT "+postFields+" from posts where YEAR(posted_on)=? and MONTH(posted_on)=? and permalink=? and private=0", archYear, archMonth, archPlink)
			} else {
				err = d.db.QueryRow("SELECT count(*) from posts where YEAR(posted_on)=? and MONTH(posted_on)=? and DAY(posted_on)=? and=0", archYear, archMonth, archDay).Scan(&PostTotal)
				rows, err = d.db.Query("SELECT "+postFields+" from posts where YEAR(posted_on)=? and MONTH(posted_on)=? and DAY(posted_on)=? order by posted_on desc and private=0", archYear, archMonth, archDay)
			}
		}
	}

	// specified: /archive/YYYY/MM/page/N
	if pathLen == 5 {
		archYear, _ := strconv.Atoi(archPath[2])
		archMonth, _ := strconv.Atoi(archPath[3])
		LinkPrefix = fmt.Sprintf("<a href=\"/archive/%v/%v/page/", archYear, archMonth)
		// archPath[4] is supposedly "page" - does not matter if it isn't since it is ignored.
		PageOffs, _ = strconv.Atoi(archPath[5])
		PostFrom = PageOffs * PostPerPage
		err = d.db.QueryRow("SELECT count(*) from posts where YEAR(posted_on)=? and MONTH(posted_on)=? and private=0", archYear, archMonth).Scan(&PostTotal)
		rows, err = d.db.Query("SELECT "+postFields+" from posts where YEAR(posted_on)=? and MONTH(posted_on)=? and private=0 order by posted_on desc limit ?,?", archYear, archMonth, PostFrom, PostPerPage)
	}

	// Paging not supported for daily query

	//  Calculate pages for footer
	// Here we also fill up the footer template which embodies the sidebars
	if PageOffs == -1 {
		pf.PageLeftLink = ""
		pf.PageRightLink = ""
	} else {
		PagePrev = strconv.Itoa(PageOffs + 1)
		PageNext = strconv.Itoa(PageOffs - 1)
		if PostTotal <= ((PageOffs + 1) * PostPerPage) {
			pf.PageLeftLink = ""
		} else {
			pf.PageLeftLink = template.HTML(LinkPrefix + PagePrev + "\">")
		}
		if PageOffs > 0 {
			pf.PageRightLink = template.HTML(LinkPrefix + PageNext + "\">")
		} else {
			pf.PageRightLink = ""
		}
	}

	// we pull the tags, cats into the footer struct
	// can go into any div after the posts are rendered
	// ie. sidebar or footer.
	pf.Tags = adminPullTags(d, 0)
	pf.Categories = adminPullCats(d, 0)


	defer rows.Close()
	if err != nil {
		log.Fatal(err)
	}
	// ---- Rendering final page - right now hardwired grid stream
	// devnull is padding for extra cols in query

	// ---- Loading Menu -- needs to be generated again if it changes
	pm.Menu = template.HTML(getMenu())


	// Query has assembled, now pull all posts into a slice (of structs)

	for rows.Next() {
		var body string
		var devnull string

		if archPath[1] == "grid" {
			rows.Scan(&tRes.Id, &tRes.Title, &body, &tRes.Permalink, &tRes.PostedOn, &tRes.PosterId, &devnull, &devnull, &devnull)
			pic_index := strings.Index(body, "</p>")
			tRes.Body = template.HTML(body[0 : pic_index+4])

		} else {
			rows.Scan(&tRes.Id, &tRes.Title, &body, &tRes.Permalink, &tRes.PostedOn, &tRes.PosterId)
			tRes.Body = template.HTML(body)
		}

		if err != nil {
			log.Fatal(err)
		}
		PostList = append(PostList, tRes)
	}

// Now iterating through the PostList - and filling it up with tags and cats
	for i := 0 ; i < len(PostList); i++ {
		PostList[i].Tags = adminPullTags(d, PostList[i].Id)
		PostList[i].Categories = adminPullCats(d, PostList[i].Id)
	}





	if archPath[1] == "grid" {
		renderTemplate(w, "view_grid", PostList, pf, pm)
	} else {
		renderTemplate(w, "view_default", PostList, pf, pm)
	}
}

var templates = template.Must(template.ParseFiles("templates/header.html",
	"templates/footer.html",
	"templates/view_default.html",
	"templates/view_grid.html",
	"templates/login.html"))

var admin_tmpl = template.Must(template.ParseFiles("admin/templates/admin_post_list.html",
	"admin/templates/admin_post_add.html",
	"admin/templates/admin_post_edit.html",
	"admin/templates/admin_image_add.html",
	"admin/templates/admin_tagcat_edit.html",
	"admin/templates/admin_view_edit.html",
	//													"admin/templates/admin_image_list.html",
	"admin/templates/admin_main.html"))

var admin_header_tmpl = template.Must(template.ParseFiles("admin/templates/admin_header.html"))
var admin_footer_tmpl = template.Must(template.ParseFiles("admin/templates/admin_footer.html"))

func renderLoginTemplate(w http.ResponseWriter, tmpl string, p []PostEntry) {
	err := templates.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func renderTemplate(w http.ResponseWriter, tmpl string, p []PostEntry, f PageFooter, m TopMenu) {
	err := templates.ExecuteTemplate(w, "header.html", m)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	err = templates.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	err = templates.ExecuteTemplate(w, "footer.html", f)
}

func renderAdminTemplate(w http.ResponseWriter, tmpl string, p []PostEntry) {
	err := admin_header_tmpl.ExecuteTemplate(w, "admin_header.html", nil)
	err = admin_tmpl.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	err = admin_footer_tmpl.ExecuteTemplate(w, "admin_footer.html", nil)
}

func renderAdminMapTemplate(w http.ResponseWriter, tmpl string, m map[string]string) {
	err := admin_header_tmpl.ExecuteTemplate(w, "admin_header.html", nil)
	err = admin_tmpl.ExecuteTemplate(w, tmpl+".html", m)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	err = admin_footer_tmpl.ExecuteTemplate(w, "admin_footer.html", nil)
}

func renderAdminEditTemplate(w http.ResponseWriter, tmpl string, p PostEntry) {
	err := admin_header_tmpl.ExecuteTemplate(w, "admin_header.html", nil)
	err = admin_tmpl.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	err = admin_footer_tmpl.ExecuteTemplate(w, "admin_footer.html", nil)
}

var validPath = regexp.MustCompile("^/(archive|grid|page|admin)/([a-zA-Z0-9]+)?(/([a-zA-Z0-9]+))?(/([a-zA-Z0-9-]+))?(/([a-zA-Z0-9-]+))?(/([a-zA-Z0-9-]+))?(/([a-zA-Z0-9-]+))?$")

/* Ok so here is the currently interpreted "map":

/admin
/admin/login
/admin/dologin
/admin/postlist
/admin/postadd

/   						(takes you to /page/0)
/page/N						(takes you to page N)
/archive  					(does nothing right now)
/archive/YYYY				(all posts made in YYYY)
/archive/YYYY/MM			(all posts made in YYYY/MM)
/archive/YYYY/MM/DD 		(all posts made on YYYY/MM/DD - specific day)
/archive/YYY/MM/perm-a-link (a specific post (it's also a permalink))

 + /page/N 					(prefix after valid URLs - implements paging)


---

Permalink format should be editable. Possible macros:

%year%
%month&
%day%
%postname&
%post-id%

These permalinks with this format could be generated for the posts as well on-the-fly.


*/

// This will be a proper URL map handler once

func makeHandler(fn func(http.ResponseWriter, *http.Request, string, *Session, *Database)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		if r.URL.Path == "/admin/" {
			http.Redirect(w, r, "/admin/login", http.StatusFound)
			//			fmt.Printf("Redirecting to /admin/login\n")
			return
		}

		if r.URL.Path == "/" {
			http.Redirect(w, r, "/page/0", http.StatusFound)
			//			fmt.Printf("Redirecting to starting page\n")
			return
		}

		s := SSession
		m := validPath.FindStringSubmatch(r.URL.Path)

		// ATTACK VECTOR - FindStringSubMatch should return a slice with length.
		// If it doesn't then the path is not valid based on the regex.

		if len(m) == 0 {
			fmt.Printf("[warning] Serving 404 for request from %v URL path failed to regex: %v\n", r.RemoteAddr, r.URL.Path)
			http.NotFound(w, r)
			return
		}
		if strings.HasPrefix(m[0], "/admin/") {
			fn(w, r, m[2], s, d)
			return
		}

		if m == nil {
			http.NotFound(w, r)
			return
		}
		fmt.Printf("[access] Serving %v to %v\n", r.URL.Path, r.RemoteAddr)
		fn(w, r, m[0], s, d)
	}
}

func serveSingle(pattern string, filename string) {
	http.HandleFunc(pattern, func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, filename)
	})
}

func main() {
	loadConfig()

	// -------- Config hack
	PostPerPage = 5
	// --------

	SSession = &Session{}

	d = &Database{
		Host:   Config["DBHost"],
		Port:   Config["DBPort"],
		User:   Config["DBUser"],
		Pass:   Config["DBPass"],
		Scheme: Config["DBScheme"]}

	err := d.init()
	if err != nil {
		fmt.Printf("Database initialization failed!\n")
	}

	err = d.create()

	flag.Parse()

	//	chttps := http.NewServeMux()

	http.HandleFunc("/", makeHandler(pageHandler))
	http.HandleFunc("/archive/", makeHandler(pageHandler))
	http.HandleFunc("/page/", makeHandler(pageHandler))
	http.HandleFunc("/admin/", makeHandler(adminHandler))
	//	chttps.HandleFunc("/admin/", makeHandler(adminHandler))

	postFields = "id, title, body, permalink, posted_on, poster_id"

	/*----------------------------------------------
	Static handlers from PageBase root.
	This can be replaced by using a map for its entries.
	For now static strings will do but when theming will be in use
	just remember to use a map[string]string.
	*/

	fs := http.FileServer(http.Dir("."))
	http.Handle("/js/", fs)
	http.Handle("/images/", fs)
	http.Handle("/css/", fs)
	http.Handle("/static/", fs)
	http.Handle("/fonts/", fs)

	//	chttps.Handle("/admin/js/", fs)
	http.Handle("/admin/js/", fs)

	serveSingle("/style.css", "./style.css")

	serveSingle("/favicon.ico", "./favicon.ico")
	serveSingle("/admin/login.css", "./admin/login.css")
	serveSingle("/admin/dashboard.css", "./admin/dashboard.css")
	serveSingle("/admin/bootstrap.min.css", "./admin/bootstrap.min.css")
	serveSingle("/admin/ie10-viewport-bug-workaround.css", "./admin/ie10-viewport-bug-workaround.css")

	//	chttps.Handle("/admin/style_admin.css", http.FileServer(http.Dir("./")))

	//---------------------------------------------


	loadMenu()


	if *addr {
		l, err := net.Listen("tcp", "127.0.0.1:0")
		if err != nil {
			log.Fatal(err)
		}
		err = ioutil.WriteFile("final-port.txt", []byte(l.Addr().String()), 0644)
		if err != nil {
			log.Fatal(err)
		}
		s := &http.Server{}
		s.Serve(l)
		return
	}

	http.ListenAndServe(":8080", nil)

	/* one of the ListeAndServe's has to be a goroutine since they never return
	err = http.ListenAndServeTLS(":443", PUBLIC_KEY, PRIV_KEY, chttps)
	if err != nil {
		log.Fatal(err)
	}
	*/
}
